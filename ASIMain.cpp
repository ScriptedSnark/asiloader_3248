﻿/**
 * Copyright - ScriptedSnark, 2024.
 * dllmain.cpp - ASI loader entry point
 *
 * Project (ASILoader_3248) header file
 * Authors: ScriptedSnark.
 * Do not delete this comment block. Respect others' work!
 */

 //WINDOWS
#include <windows.h>

//STL
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <functional>
#include <array>
#include <random>
#include <fstream>
#include <thread>

void* g_lpClient;
typedef void (*PluginMainFunc)();

std::vector<HMODULE> g_pluginList;
bool g_bLoaded = false;

void UnloadPlugins();
void LoadPlugins();

#undef GetCurrentDirectory
std::string GetCurrentDirectory()
{
	char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");

	return std::string(buffer).substr(0, pos);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void InitializePlugin(HMODULE hPlugin, const char* szName)
{
	PluginMainFunc pluginMain = (PluginMainFunc)::GetProcAddress(hPlugin, "ASIMain");

	if (pluginMain)
	{
		printf("Initializing plugin - %s...\n", szName);
		CreateThread(NULL, NULL, reinterpret_cast<LPTHREAD_START_ROUTINE>(pluginMain), NULL, NULL, NULL); // start another thread running the hooking stuff

		g_pluginList.push_back(hPlugin);
	}
	else
		printf("Error: failed to initialize plugin - %s\n", szName);
}

//-----------------------------------------------------------------------------
// Purpose: load ASI plugins
//-----------------------------------------------------------------------------
void LoadPlugins()
{
	if (g_bLoaded)
		return;

	g_pluginList.clear();

	std::vector<std::string> pluginFiles;
	std::string pluginsDir = GetCurrentDirectory() + '\\' + "asi_plugins";
	printf("LoadPlugins: %s\n", pluginsDir.c_str());

	WIN32_FIND_DATAA findData;
	HANDLE hFind = FindFirstFileA((pluginsDir + "\\*.asi").c_str(), &findData);

	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			pluginFiles.push_back(pluginsDir + "\\" + findData.cFileName);
		} while (FindNextFileA(hFind, &findData));

		FindClose(hFind);
	}
	else
	{
		printf("Error: failed to find plugin files in %s\n", pluginsDir.c_str());
		return;
	}

	for (const std::string& pluginFile : pluginFiles)
	{
		HMODULE plugin = ::LoadLibraryA(pluginFile.c_str());

		if (plugin)
			InitializePlugin(plugin, pluginFile.c_str());
		else
			printf("Error: failed to load plugin - %s\n", pluginFile.c_str());
	}

	g_bLoaded = true;
}

//-----------------------------------------------------------------------------
// Purpose: unload ASI plugins
//-----------------------------------------------------------------------------
void UnloadPlugins()
{
	if (!g_bLoaded)
		return;

	for (HMODULE& pluginHandle : g_pluginList)
	{
		if (pluginHandle)
		{
			PluginMainFunc pluginShutdown = (PluginMainFunc)::GetProcAddress(pluginHandle, "ASIShutdown");

			if (pluginShutdown)
				pluginShutdown();

			::FreeLibrary(pluginHandle);
		}
	}

	g_pluginList.clear();
	g_bLoaded = false;
}


//-----------------------------------------------------------------------------
// Purpose: load plugins in right time 
//-----------------------------------------------------------------------------
void WaitUntilClientLoads()
{
lbl_waitFor:
	g_bLoaded = false;
	g_lpClient = GetModuleHandleA("client");

	if (g_lpClient)
		LoadPlugins();
	else
		goto lbl_waitFor;
}

void ASILoader_Main()
{
	AllocConsole();
	FILE* in, * out;
	freopen_s(&in, "conin$", "r", stdin);
	freopen_s(&out, "conout$", "w+", stdout);

	printf("ASI loader for GoldSrc 3XXX builds | Build time: " __TIMESTAMP__ "\n");
	
	UnloadPlugins();
	std::thread t(WaitUntilClientLoads);
	t.detach();
}

void ASILoader_Shutdown()
{
	printf("Shutting down ASI loader...\n");
	UnloadPlugins();
}

 /*
 ================
 DllMain
 ================
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	if (fdwReason == DLL_PROCESS_ATTACH)
	{
		//DisableThreadLibraryCalls(hinstDLL); // said to increase performance?
		CreateThread(NULL, NULL, reinterpret_cast<LPTHREAD_START_ROUTINE>(ASILoader_Main), NULL, NULL, NULL); // start another thread running the hooking stuff
		return TRUE;
	}

	return FALSE;
}